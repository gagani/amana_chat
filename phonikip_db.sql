-- Adminer 4.8.0 MySQL 10.5.10-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `AgentChat`;
CREATE TABLE `AgentChat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(30) NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `message` varchar(200) NOT NULL,
  `ChatType` varchar(200) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `AgentChatNotification`;
CREATE TABLE `AgentChatNotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` varchar(300) NOT NULL,
  `datetime` datetime NOT NULL,
  `publisher` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2021-06-03 11:37:14
