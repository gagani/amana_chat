
<!DOCTYPE html>
<?php
use Illuminate\Support\Facades\Route;
require app_path() . '/Http/Helpers/helpers.php';
?>
<html>
    <head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>NEXT CONNECT</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- favicon
		    ============================================ -->
	<link rel="shortcut icon" href="{{ asset('public/img/logo/fevicon.png') }}" type="image/png">
	<!-- Google Fonts
		    ============================================ -->
	<!-- Bootstrap CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/css/common_style.css') }}">
	<!-- Bootstrap CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}" >
	<!-- owl.carousel CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/owl.carousel.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/css/owl.theme.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/css/owl.transitions.css') }}" >
	<!-- animate CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/animate.css') }}" >
	<!-- normalize CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/normalize.css') }}" >
	<!-- meanmenu icon CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/meanmenu.min.css') }}"  >
	<!-- main CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/main.css') }}" >
	<!-- buttons CSS
	       ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/buttons.css') }}" >
	<!-- educate icon CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/educate-custon-icon.css') }}">
	<!-- morrisjs CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/morrisjs/morris.css') }}" >
	<!-- mCustomScrollbar CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/scrollbar/jquery.mCustomScrollbar.min.css') }}" >
	<!-- metisMenu CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/metisMenu/metisMenu.min.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/css/metisMenu/metisMenu-vertical.css') }}">
	<!-- calendar CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/calendar/fullcalendar.min.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/css/calendar/fullcalendar.print.min.css') }}" >
	<!-- style CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/custom_css/style.css') }}" >
	<!-- select2 CSS
		============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/select2/select2.min.css') }}" >
	<!-- chosen CSS
		============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/chosen/bootstrap-chosen.css') }}" >
	<!-- ionRangeSlider CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/ionRangeSlider/ion.rangeSlider.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/css/css/ionRangeSlider/ion.rangeSlider.skinFlat.css') }}" >
	<!-- responsive CSS
		   ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}">
	<!-- datapicker CSS
		============================================ -->
	<!--<link rel="stylesheet" href="{{ asset('public/css/datapicker/datepicker3.css') }}" >-->
	<!-- forms CSS
	    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/form/themesaller-forms') }}" >
	<!-- x-editor CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/editor/select2.css') }}" >
	<!--<link rel="stylesheet" href="{{ asset('public/css/editor/datetimepicker.css') }}" >-->
	<link rel="stylesheet" href="{{ asset('public/css/editor/bootstrap-editable.css') }}">
	<link rel="stylesheet" href="{{ asset('public/css/editor/x-editor-style.css') }}" >
	<!-- normalize CSS
		    ============================================ -->
	<link rel="stylesheet" href="{{ asset('public/css/data-table/bootstrap-table.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/css/data-table/bootstrap-editable.css') }}" >
	<!-- style CSS
	    ============================================ -->
	<!-- DataTables -->
	<link href="{{ asset('public/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">   
	<link href="{{ asset('public/css/buttons.dataTables.min.css') }}" rel="stylesheet">
	<!-- multi select -->
	<link href="{{ asset('public/css/multi-select.css') }}" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{ asset('public/custom_css/WhatsappStyle.css') }}">


	<!-- modernizr JS
		    ============================================ -->
	<script src="{{ asset('public/dist_01/js/pdfmake.js') }}"></script>
	<script src="{{ asset('public/js/vendor/modernizr-2.8.3.min.js') }}" ></script>
	<!-- jquery
		    ============================================ -->
        <script src="{{ asset('public/js/vendor/jquery-1.12.4.min.js') }}" ></script>
	<!-- bootstrap JS
		    ============================================ -->
	<script src="{{ asset('public/js/bootstrap.min.js') }}" ></script>
	<!-- wow JS
		    ============================================ -->
	<script src="{{ asset('public/js/wow.min.js') }}" ></script>
	<!-- price-slider JS
		    ============================================ -->
	<script src="{{ asset('public/js/jquery-price-slider.js') }}" ></script>
	<!-- meanmenu JS
		    ============================================ -->
	<script src="{{ asset('public/js/jquery.meanmenu.js') }}"></script>
	<!-- owl.carousel JS
		    ============================================ -->
	<script src="{{ asset('public/js/owl.carousel.min.js') }}" ></script>
	<!-- sticky JS
		    ============================================ -->
	<script src="{{ asset('public/js/jquery.sticky.js.js') }}"></script>
	<!-- scrollUp JS
		    ============================================ -->
	<script src="{{ asset('public/js/jquery.scrollUp.min.js') }}" ></script>
	<!-- counterup JS
		    ============================================ -->
	<script src="{{ asset('public/js/counterup/jquery.counterup.min.js') }}" ></script>
	<script src="{{ asset('public/js/counterup/waypoints.min.js') }}" ></script>
	<script src="{{ asset('public/js/counterup/counterup-active.js') }}"></script>
	<!-- mCustomScrollbar JS
		    ============================================ -->
	<script src="{{ asset('public/js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}" ></script>
	<script src="{{ asset('public/js/scrollbar/mCustomScrollbar-active.js') }}" ></script>
	<!-- metisMenu JS
		    ============================================ -->
	<script src="{{ asset('public/js/metisMenu/metisMenu.min.js') }}" ></script>
	<script src="{{ asset('public/js/metisMenu/metisMenu-active.js') }}" ></script>
	<!-- morrisjs JS
		    ============================================ -->
	<script src="{{ asset('public/js/morrisjs/raphael-min.js') }}" ></script>
	<script src="{{ asset('public/js/morrisjs/morris.js') }}" ></script>
	<script src="{{ asset('public/js/morrisjs/morris-active.js') }}"></script>
	<!-- morrisjs JS
		    ============================================ -->
	<script src="{{ asset('public/js/sparkline/jquery.sparkline.min.js') }}" ></script>
	<script src="{{ asset('public/js/sparkline/jquery.charts-sparkline.js') }}" ></script>
	<script src="{{ asset('public/js/sparkline/sparkline-active.js') }}" ></script>
	<!-- calendar JS
		    ============================================ -->
	<script src="{{ asset('public/js/calendar/moment.min.js') }}" ></script>
	<script src="{{ asset('public/js/calendar/fullcalendar.min.js') }}" ></script>
	<script src="{{ asset('public/js/calendar/fullcalendar-active.js') }}"></script>
	<!-- plugins JS
		    ============================================ -->
	<script src="{{ asset('public/js/plugins.js') }}" ></script>
	<!-- main JS
		    ============================================ -->
	<script src="{{ asset('public/js/main.js') }}" ></script>

	<!-- Charts JS
	       ============================================ -->
	<script src="{{ asset('public/js/charts/Chart.js') }}" ></script>
	<script src="{{ asset('public/js/charts/rounded-chart.js') }}" ></script>
	<!-- Charts JS
		============================================ -->
	<script src="{{ asset('public/js/charts/bar-chart.js') }}" ></script>
	<!-- data table JS
		    ============================================ -->
	<script src="{{ asset('public/js/data-table/bootstrap-table.js') }}" ></script>
	<script src="{{ asset('public/js/data-table/tableExport.js') }}" ></script>
	<script src="{{ asset('public/js/data-table/data-table-active.js') }}" ></script>
	<script src="{{ asset('public/js/data-table/bootstrap-table-editable.js') }}" ></script>
	<script src="{{ asset('public/js/data-table/bootstrap-editable.js') }}" ></script>
	<script src="{{ asset('public/js/data-table/bootstrap-table-resizable.js') }}" ></script>
	<script src="{{ asset('public/js/data-table/colResizable-1.5.source.js') }}"></script>
	<script src="{{ asset('public/js/data-table/bootstrap-table-export.js') }}"></script>
	<!--  editable JS
		    ============================================ -->
	<script src="{{ asset('public/js/editable/jquery.mockjax.js') }}" ></script>
	<script src="{{ asset('public/js/editable/mock-active.js') }}" ></script>
	<script src="{{ asset('public/js/editable/select2.js') }}" ></script>
	<script src="{{ asset('public/js/editable/moment.min.js') }}" ></script>
	<!--<script src="{{ asset('public/js/editable/bootstrap-datetimepicker.js') }}" ></script>-->
	<script src="{{ asset('public/js/editable/bootstrap-editable.js') }}" ></script>
	<script src="{{ asset('public/js/editable/xediable-active.js') }}" ></script>
	<!-- Chart JS
		    ============================================ -->
	<script src="{{ asset('public/js/chart/jquery.peity.min.js') }}" ></script>
	<script src="{{ asset('public/js/peity/peity-active.js') }}"></script>
	<!-- touchspin JS
	       ============================================ -->
	<script src="{{ asset('public/js/touchspin/jquery.bootstrap-touchspin.min.js') }}" ></script>
	<script src="{{ asset('public/js/touchspin/touchspin-active.js') }}" ></script>
	<!-- colorpicker JS
		    ============================================ -->
	<script src="{{ asset('public/js/colorpicker/jquery.spectrum.min.js') }}" ></script>
	<script src="{{ asset('public/js/colorpicker/color-picker-active.js') }}" ></script>
	<!-- datapicker JS
		    ============================================ -->
	<!-- input-mask JS
		============================================ -->
	<script src="{{ asset('public/js/input-mask/jasny-bootstrap.min.js') }}" ></script>
	<!-- chosen JS
		    ============================================ -->
	<script src="{{ asset('public/js/chosen/chosen.jquery.js') }}" ></script>
	<script src="{{ asset('public/js/chosen/chosen-active.js') }}" ></script>
	<!-- select2 JS
		    ============================================ -->
	<script src="{{ asset('public/js/select2/select2.full.min.js') }}" ></script>
	<script src="{{ asset('public/js/select2/select2-active.js') }}" ></script>
	<!-- ionRangeSlider JS
		    ============================================ -->
	<script src="{{ asset('public/js/colorpicker/jquery.spectrum.min.js') }}" ></script>
	<script src="{{ asset('public/js/colorpicker/jquery.spectrum.min.js') }}" ></script>
	<!-- rangle-slider JS
		    ============================================ -->
	<script src="{{ asset('public/js/rangle-slider/jquery-ui-1.10.4.custom.min.js') }}" ></script>
	<script src="{{ asset('public/js/rangle-slider/jquery-ui-touch-punch.min.js') }}" ></script>
	<script src="{{ asset('public/js/rangle-slider/rangle-active.js') }}" ></script>
	<!-- knob JS
		    ============================================ -->
	<script src="{{ asset('public/js/knob/jquery.knob.js') }}" ></script>
	<script src="{{ asset('public/js/knob/knob-active.js') }}" ></script>
	<!--<script src="{{ asset('public/js/datapicker/bootstrap-datepicker.js') }}" ></script>
	<script src="{{ asset('public/js/datapicker/datepicker-active.js') }}" ></script>-->
	<!-- tab JS
		    ============================================ -->
	<script src="{{ asset('public/js/tab.js') }}" ></script>

	<script src="{{ asset('public/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('public/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js') }}"></script> 
	<link href="{{ asset('public/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">  
	<!-- DataTables -->
	<script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/js/dataTables.buttons.min.js') }}"></script>
	<!------*********** Added By badra to view excelHtml5 button 29.01.2020************----------->
	<script src="{{ asset('public/js/jszip.min.js') }}"></script>
	<script src="{{ asset('public/js/buttons.html5.min.js') }}"></script>
	<!------*********** Added By badra to view excelHtml5 button 29.01.2020************----------->

	<!-- multi select -->
	<script src="{{ asset('public/js/jquery.multi-select.js') }}"></script>
	
	<script src="{{ asset('public/js/css_menu_script.js') }}"></script>
	<script src="{{ asset('public/js/bootbox.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('public/css/css_menu_styles.css') }}" >
	<link rel="stylesheet" href="{{ asset('public/custom_css/Agentchat.css') }}" >
	<script> 
	//$(document).ready(function(){
	// setTimeout(function(){                                                                                     
	//     var userId='<?php //echo Session::get('userid')  ?>';
	//         $.ajax({
	//                         url: 'changeLoginStatus',
	//                         type: 'GET',
	//                         data: {userId: userId},
	//                         success: function (response)
	//                         {
	//                             location.href = "{{url ('loginout')}}";
	//                         }
	//                         });
	//         //YOUR CODE

	// },1800000);
	/*});*/
	</script>
</head>

<body>
	<div class="topnav" id="myTopnav">
		<div id='cssmenu'>
			<ul>
				<li style="width: 15%;">
					<div class="logo" style="width: 100%; margin-top: 20px;"><a  href="{{url ('Agentdashboard')}}">
						<img src="{{ asset('public/img/logo/new_logo.png') }}" alt=""/> </a> 
					</div>
				</li> 
				<?php if (session()->get('usertypeid') ==17 || session()->get('usertypeid') ==1){ ?>
					<li><a target="_blank" href="http://<?php echo $_SERVER['SERVER_ADDR']?>:5000/amanabank">Live Dashboard<span class="sr-only">(current)</span></a></li>
					<li class='active'><a href='#'>Help Desk</a>
						<ul style="z-index: 1;">
							<li><a href="{{url ('lvlonedashboard')."?contactid=all&status=All"}}">Service Unit db</a></li>
							<li><a href="{{url ('lvltwodashboard')."?contactid=all&status=All"}}">Location db</a></li>
						    <li><a href="{{url ('lvlthrdashboard')."?contactid=all&status=All"}}">Category db</a></li>
						    <li><a href="{{url ('lvlthragentdashboard')."?contactid=all&status=All"}}">Agent db</a></li>
						    <li><a href="{{url ('emailsrvdashboard')}}">Email SRV db</a></li>
						    <li><a href="{{url ('archemailsrvdashboard')}}">Archieve Email SRV db</a></li>
						    <li><a href="{{url ('smssrvdashboard')}}">SMS SRV db</a></li>
						    <li><a href="{{url ('archsmssrvdashboard')}}">Archieve SMS SRV db</a></li>
						    <li><a href="{{url ('faxsrvdashboard')}}">FAX SRV db</a></li>
						    <li><a href="{{url ('archfaxsrvdashboard')}}">Archive FAX SRV db</a></li>
						    <li><a href="{{url ('WhatsAppsrvDashboard')}}">WhatsApp SRV db</a></li>
						    <li><a href="{{url ('ArchiveWhatsAppsrvDashboard')}}">Archive WhatsApp SRV db</a></li>
						    <li><a href="{{url ('WebChatsrvDashboard')}}">WebChat SRV db</a></li>
						    <li><a href="{{url ('ArchiveWebChatsrvDashboard')}}">Archive WebChat SRV db</a></li>
						    <li><a href="{{url ('remainderdashboard')}}">Reminder db</a></li>
						    <li><a href="{{url ('incompleterecdashboard')}}">Incomplete Records</a></li>
						    <li><a href="{{url ('ticketTransferDashboard')."?contactid=all&status=All"}}">Reallocation</a></li>
						    <li><a href="{{url ('toBeCompleteDashBoard')."?contactid=all&status=All"}}">Followed-ups</a></li>
						</ul>
					</li>
					<li><a href="{{url ('contacts')."?contactid=all&status=All"}}">Contact Register</a></li>
				<?php } ?>
				<?php if (session()->get('usertypeid') ==17 || session()->get('usertypeid') ==1){ ?>
					<li class='active'><a href='#'>Help Desk Reports</a>
						<ul style="z-index: 1;">
							<li><a href="{{url ('TicketdetailsDashboard')."?contactid=all&status=All"}}">Ticket Details Report</a></li>
						</ul>
					</li>
					<li class='active'><a href='#'>Calling Campaign</a>
						<ul style="z-index: 1;">
							<!--<li><a href="{{url ('uploadcsv')}}">Mo/Re List Upload</a></li>
						    <li><a href="{{url ('uploadcsvP')}}">Payment List Upload</a></li>-->
							<li><a href="{{url ('uploadcsv')}}">Excel Data Upload</a></li>
						    <li><a href="{{url ('campaigns')}}">Campaign Manager</a></li>
						    <li><a href="{{url ('CampaignDataDownload')}}">Campaign Data Download</a></li>
						    <li><a href="{{url ('donotCallNos')}}">Do Not Call</a></li>
						</ul>
					</li>
					<li class='active'><a href='#'>SMS Campaign</a>
						<ul>
							<li><a href="{{url ('smscampaigns')}}">Campaign manager</a></li>
						</ul>
					</li>
					<li class='active'><a href='#'>Campaign Report</a>
						<ul style="z-index: 1;">
							<li><a href="{{url ('QnreportDashboard')}}">Questionnaire Report</a></li>
						    <li><a href="{{url ('CallcampagntsummryDashboard')}}">Call Camp.summary Report</a></li>
						    <li><a href="{{url ('CallcampagntDashboard')}}">Call Camp.agent Report</a></li>
						</ul>
					</li>
				<?php } ?>
				<li class='active'><a href='#'>Call Center Reports</a>
					<ul style="z-index: 1;">
						<li> <a class="dropdown-item" href="#"> Agent Reports &raquo; </a>
							<ul class="submenu dropdown-menu">
							  	<li><a href="{{url ('view_agntactionhistory')}}">Agent Action History</a></li>
							    <li><a href="{{url ('view_agntcallhistory')}}">Agent Calls History</a></li>
								<li><a href="{{url ('view_agentaction_summ_report')}}">Agent Action Summary Report</a></li>
							    <li><a href="{{url ('view_agnt_det_report')}}">Agent Details Report</a></li>
								<li><a href="{{url ('occupancyReport')}}">Agent Occupancy Report</a></li>
								<li><a href="{{url ('view_findcallhistory')}}">Agent Call Log db</a></li>
								<li><a href="{{url ('view_agent_reschedule_list')}}">Agent Reschedule Call List</a></li>
								<li><a href="{{url ('view_br_exceed_repo')}}">Break Time Exceeded Report</a></li>
			            	</ul>
			            </li>
			            <li> <a class="dropdown-item" href="#"> Queue Reports &raquo; </a>
			            	<ul class="submenu dropdown-menu">
							  	<li><a href="{{url ('view_queue_report')}}">Queue Report</a></li>
							 	<li><a href="{{url ('view_queue_det_report')}}">Queue Details Report</a></li>
								<li><a href="{{url ('view_abncall_report')}}">Abandon Calls Report</a></li>
							    <li><a href="{{url ('abandoncallbackreport')}}">Abandon Callback Report</a></li>
								<li><a href="{{url ('view_abn_hourly_report')}}">Abandon Hourly Report</a></li>
								<li><a href="{{url ('view_queue_abn_hourly_report')}}">Queue Abandon Hourly Report</a></li>
								<li><a href="{{url ('queue_waiting_time_report')}}">Queue Waiting Time Report</a></li>
			            	</ul>
			            </li>
			            <li><a href="{{url ('callbackrequestreport')}}">Callback Request Report</a></li>
						<li><a href="{{url ('view_outboundcall_report')}}">Outgoing Calls Report</a></li>
						<li><a href="{{url ('view_calls_det_report')}}">Calls Details Report</a></li>
						<li><a href="{{url ('view_calls_trans_report')}}">Calls Transfer Report</a></li>
						<li><a href="{{url ('view_call_sum_repo')}}">Call Summary Report</a></li>
						<li><a href="{{url ('view_agnt_ring_detail_report')}}">Ringing Details Report</a></li>
						<li> <a class="dropdown-item" href="#"> IVR Reports &raquo; </a>
							<ul class="submenu dropdown-menu">
								<li><a href="{{url ('IVRreport')}}">IVR Report</a></li>
							    <li><a href="{{url ('IVRDropDetail')}}">IVR Drop Calls Report</a></li>
							    <li><a href="{{url ('IVRExtensionDial')}}">IVR Extension Dial Report</a></li>
								<li><a href="{{url ('view_call_rate_det')}}">Call Rating Detail Report</a></li>
								<li><a href="{{url ('view_call_rate_summ_agnt')}}">Call Rating Summary - Agent Report</a></li>
								<li><a href="{{url ('view_call_rate_summ_queue')}}">Call Rating Summary - Queue Report</a></li>
			            	</ul>
			            </li>
			        </ul>
			    </li>
			    <?php if (session()->get('usertypeid') ==17 || session()->get('usertypeid') ==1){ ?>
			    	<li class='active'><a href='#'>System Setup</a>
			    		<ul style="z-index: 1;">
		          			<li><a href="{{url ('viewlevelone')}}">Service Unit</a></li>
						    <li><a href="{{url ('viewleveltwo')}}">Location</a></li>
						    <li><a href="{{url ('viewlevelthr')}}">Category</a></li>
						    <li><a href="{{url ('view_cat_list')}}">call log type 1</a></li>
						    <li><a href="{{url ('view_inq_list')}}">call log type 2</a></li>
						    <li><a href="{{url ('viewkpi_info')."?contactid=all&status=All"}}">KPI Level Info</a></li>
						    <li><a href="{{url ('levelWiseUserAllocation')}}">CSP Supervisors</a></li>
						    <li><a href="{{url ('levelWiseaAgentAllocation')}}">CSP Users</a></li>
						    <li><a href="{{url ('srvemailAgentAllocation')}}">SRV Users</a></li>
						    <li><a href="{{url ('massegeTemplates')}}">Email Templates</a></li>
						    <li><a href="{{url ('emailAttachment')}}">Email Attachments</a></li>
						</ul>
					</li>
					<li class='active'><a href='#'>Administration</a>
						<ul style="z-index: 1;">
							<li><a href="{{url ('users')}}">Users</a></li>
						    <li><a href="{{url ('usertype')}}">User Types</a></li>
						    <li><a href="{{url ('view_sipreg')}}">SIP Registration</a></li>
						    <li><a href="{{url ('viewQueList')}}">Queue Extension Allocation</a></li>
						    <!-- <li><a href="{{url ('view_queue_allo_company')}}">Queue Allocation to Company</a></li>
						    <li><a href="{{url ('view_sup_allo_company')}}">Supervisor Allocation to Company</a></li>
							<li><a href="{{url ('view_queue_allo_tmldr')}}">Queue Allocation to Team Leader</a></li>
							<li><a href="{{url ('view_agnt_allo_tmldr')}}">Agent Allocation to Team Leader</a></li>
							<li><a href="{{url ('view_sup_user_allo_com')}}">Superuser Allocation to company</a></li> -->
							<li><a href="{{url ('view_extension_list')}}">Skills List</a></li>
						    <li><a href="{{url ('csp_users')}}">Agent Log Off Reset</a></li>
						    <li><a href="{{url ('view_phone_bk')}}">Phone Book</a></li>
						    <li><a href="{{url ('Cspuser_allocation')}}">CSP User Types Allocation</a></li>
						    <li><a href="{{url ('privilagetype')}}">User Privilleges</a></li>
						    <li><a href="{{url ('presetremarks')}}">Preset Remarks</a></li>
						    <li><a href="{{url ('view_srvyqn')}}">Survey Questions</a></li>
						    <li><a href="{{url ('view_srvyqngrp')}}">Questionnaire</a></li>
							<li><a href="{{url ('view_useraction')}}">User Activity Log</a></li>
						</ul>
					</li>
					<li class='active'><a href='#'>Evaluation Contexts</a>
				      	<ul style="z-index: 1;">
							<li><a href="{{url ('view_eval_crit_type_list')}}">Evaluation Criteria Type Master</a></li>
							<li><a href="{{url ('view_eval_crit_mst_list')}}">Evaluation Criteria Master</a></li>
							<li><a href="{{url ('view_eval_cntxt')}}">Evaluation Context Master</a></li>
							<li><a href="{{url ('view_agnt_eval_mst_list')}}">Agent Evaluation Master</a></li>
							<li><a href="{{url ('view_agnt_quality_eval_list')}}">Agent Quality Evaluation</a></li>
							<li><a href="{{url ('view_agnt_eval_rate_list')}}">Agent Evaluation Rate Report</a></li>
				    	</ul>
				   </li>
				<?php } ?>
				<li class='active' style="float:right;"><a href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></a>
					<ul style="z-index: 1;" class="new">
						<!--<li><a href="#">Settings</a></li>-->
						<li><a href="{{url ('resetpassword')}}">Reset Password</a></li>
						<li><a onclick="logoutpopup();">Logout</a></li>
					</ul>
				</li>
				<li class='active' style="float:right; width: 2%; margin-top:15px;">
					<div class="notification"><a href='#'>
						<?php
						$faxCount = getNotificationCountFax();
						?> 
						<i class="fa fa-fax faxicon js_opacity"  aria-hidden="true"></i>
						<div class="badge hidden"  id ="fax">
							<?php
							if(!empty($faxCount[0]->faxQty)){
								echo $faxCount[0]->faxQty;
							}
							?>
						</div>
					</a></div>
				</li>
				<li class='active' style="float:right; width: 2%; margin-top:15px;">
					<div class="notification"><a href='#'>
						<?php
						$emailCount = getNotificationCountEmail();
						?>
						<i class="fa fa-envelope-o envicon js_opacity"  aria-hidden="true"></i>
						<div  class="badge hidden" id ="email">
							<?php
							if (!empty($emailCount[0]->emailQty)) {
								echo $emailCount[0]->emailQty;
							}
							?>
						</div>
					</a></div>
				</li>
				<li class='active' style="float:right; width: 2%; margin-top:15px;">
					<div class="notification">
						<a href='#'>
							<?php
							$smsCount = getNotificationCountSms();
							?>
							<i class="fa fa-commenting-o faxmsgicon js_opacity"  aria-hidden="true"></i>
						</a>
						<div  class="badge hidden" id ="sms" >
							<?php
							if (!empty($smsCount[0]->smsQty)) {
								echo $smsCount[0]->smsQty;
							}
							?>
						</div>
					</div>
				</li>
				<li class='active' style="float:right; width: 2%; margin-top:15px;">
					<div type="button" onclick="goreminder();"class="notification"><a href='#'>
						<?php
						$remCount = getNotificationCountRem();
						?>
						<i class="fa fa-bell-o remicon js_opacity"  aria-hidden="true"></i>
						<div  class="badge hidden" id ="rem" >
							<?php
							if (!empty($remCount[0]->remQty)) {
								echo $remCount[0]->remQty;
						    }
						    ?>
						</div>
					</a></div>
				</li>
			</ul>
		</div>
	</div>

	<!-- <button class="open-button" onclick="openForm()">Chat</button> -->
	<a onclick="openForm()" class="float"><i class="fa fa-plus my-float"></i><sup>&nbsp;<span id="alertAll" style="color:red"></span></sup></a>

	<div class="chat-popup col col-lg-4" id="ChatPopup">
		<div class="container-md">	 
			<div class="row align-items-center">   	
				<div id="chatTypeWrap">	
					<div class="col-6">
						<button class="btn btn-primary btn-lg buttonStylecus" id="PrivateButton">Private Chat <sup><span id="alertPrivate" style="color:red"></span></sup></button>
						<button class="btn btn-primary btn-lg buttonStylecus" id="groupchatButton">Group Chat <sup><span id="alertGroup" style="color:red"></span></sup></button>
						<button class="btn btn-primary btn-lg buttonStylecus" id="NotificationButton">Notification <sup><span id="alertNotification" style="color:red"></sup></span></button>
						
						<button class="buttonclosechat" onclick="closeForm()" aria-label="Close">&times;</button>
					</div>
				</div>
				<hr>
			</div>
			<div class="row top-buffer"></div>
			<div class="row align-items-center">
				<div class="col-5">
					<div id="notificationWrap">
						<form id="Notificationform" action="">

							<div class="notifiwrap" id="notificwrap">
								<div name="oldnotificationtxt" id="oldnotificationtxt"></div>
								<div name="notificationtxt" id="notificationtxt"></div>
								<div name="notificationError" id="notificationError" class="errormsgchat"></div>
							</div>
							
				            <textarea id="Notificationinput" class="form-control customnotifiInput" autocomplete="off" rows="3" ></textarea>
				            <button class="btn btn-primary buttonStylesendcusNot"><i class="fa fa-paper-plane-o fa-lg" aria-hidden="true"></i></button>
				        </form>
					</div>
				</div>
			</div>		
			<div class="row col-4">
				<div id="ChatWrap">
					<div class="card col-sm-4 align-self-start customReg">			
						<div id="RegisteredList" class=""></div>
						<div class="vl"></div>
					</div>
					
					<div class="align-self-end" style="background-color: white;">
	          			<div class="chatbody">
	          				<label class="chatPerson" id="PersonChat"></label>
							<div id="chatPublicDiv">			
								<div id="chatDiv"></div>
								<div class="messagedivcls" id="messagesWrap">
									<div class="sub col-3">
										<div id="oldermsg"></div>
										<div id="messages"></div>
										<div id="errorsinChat" class="errormsgchat"></div>
									</div>		
																
								</div>
								<div class="messagedivcls" id="PrivatemessagesWrap">
									<div class="sub col-3">
										<div id="prvivateoldermsg"></div>
										<div id="privatemessages"></div>
										<div id="errorsinChat" class="errormsgchat"></div>
									</div>		
																
								</div>
								<div id="SendmsgWrap" class="sendmessageWrap">	            						
									<input type="hidden" name="usernameid" id="usernameid" value="<?php echo session()->get('username');  ?>">
							        <form id="Messageform" action="">
							        	<div class="col-sm-6">
							            	<input id="input" class="form-control custominput" autocomplete="off" />
							            </div>
							            <div class="col-xs-3">
							            	<button class="btn btn-primary buttonStylesendcus"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
							            </div>
							        </form>
						        </div>
					        </div>
					    </div>
					</div>
		    	</div>
		    </div>	    
		</div>
	</div>


        <!-- <script src="http://192.168.1.88:3000/socket.io/socket.io.js"></script> -->
        <script src="http://{{ Request::getHost() }}:3000/socket.io/socket.io.js"></script>

	</div>

	<script>
		function openForm() {
		  document.getElementById("ChatPopup").style.display = "block";
		  $("#alertAll").removeClass('dot');
		}

		function closeForm() {
		  document.getElementById("ChatPopup").style.display = "none";
		  $("#alertAll").removeClass('dot');
		}
	</script>

	<script>

		$(document).ready(function () {

			$("#ChatWrap").hide();
			$("#notificationWrap").hide();

			var element = document.getElementById("messagesWrap");
			element.scrollTop = element.scrollHeight;

			$.ajax({

	           type:'GET',
	           url:'{{url("GetOnlineAgents")}}',
	           data: {} ,
	           success:function(data) {

	           		var body = '';

	              	$.each(data.Getusers, function (index, c) {

	              		// $( "#usersList" ).append($('<option>').val(c.id).text(c.username));
	              		
	             	});

	           }
	        });

		});


        const socket = io(document.location.protocol+'//'+document.location.host+':3000', {transports: ['websocket']}, { autoConnect: false })
        const messageContainer = document.getElementById('messages')
        const pvtmessageContainer = document.getElementById('privatemessages')
        const oldmessageContainer = document.getElementById('oldermsg')
        const pvtoldmessageContainer = document.getElementById('prvivateoldermsg')
        const chatDiv = document.getElementById('chatDiv')
        const registeredList = document.getElementById('RegisteredList')
        const messageForm = document.getElementById('Messageform')
        const messageInput = document.getElementById('input')
        const name = document.getElementById('usernameid').value

        // Notification Elements

        const notificationform = document.getElementById('Notificationform')
        const notificationContainer = document.getElementById('notificationtxt')
        const oldnotificationContainer = document.getElementById('oldnotificationtxt')
        const notificationinput = document.getElementById('Notificationinput')


        appendMessage('You joined')
        socket.emit('new-user', name, function(data){

        })

        var TypeChat = '';

   //      function openPVTmesg() {

   //      	TypeChat = 'pvt';
   //      	document.getElementById("ChatWrap").style.display = "block";
   //      	document.getElementById("notificationWrap").style.display = "none";
   //      	document.getElementById("PersonChat").style.display = "none";
   //      	document.getElementById("messagesWrap").style.display = "none";
   //      	document.getElementById("SendmsgWrap").style.display = "none";

   //      	$("#alertPrivate").removeClass('dot');
		 //  	// $('#chatTypeWrap').hide();

		 //  	socket.emit('chatType', TypeChat, function(data){

			// });
   //      }

        
		$( "#PrivateButton" ).click(function() {
			TypeChat = 'pvt'
			// alert(TypeChat);
		  	// $('#ChatWrap').show();
		  	$('#ChatWrap, #RegisteredList').css('display', 'block');
		  	$("#notificationWrap, #PrivatemessagesWrap, #PersonChat, #SendmsgWrap, #messagesWrap").css('display', 'none');
		  	$("#PrivateButton").addClass('active');
		  	$("#alertPrivate").removeClass('dot');
		  	// $('#chatTypeWrap').hide();

		  	socket.emit('chatType', TypeChat, function(data){

			});
		  	$("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());
			
		});

		$( "#groupchatButton" ).click(function() {
			TypeChat = 'pbl'
			// alert(TypeChat);
		  	$('#ChatWrap, #messagesWrap').css('display', 'block');
		  	$("#notificationWrap, #PersonChat, #RegisteredList, #PrivatemessagesWrap").css('display', 'none');	
		  	$('#messages').empty();
	        $('#oldermsg').empty();	  	
		  	// $('#chatTypeWrap').hide();
		  	$("#alertGroup").removeClass('dot');
		  	$("#groupchatButton").addClass('active');

		  	socket.emit('chatType', TypeChat, function(data){

			});
			$("#messagesWrap").scrollTop($("#messagesWrap").height());
		});

		$( "#NotificationButton" ).click(function() {
			TypeChat = 'group'
			// alert(TypeChat);
		  	$("#notificationWrap").css('display', 'block');
		  	$('#ChatWrap').css('display', 'none');
		  	$('#notificationtxt').empty();
		  	$('#oldnotificationtxt').empty();
		  	$("#alertNotification").removeClass('dot');
		  	$("#NotificationButton").addClass('active');
		  	
		  	socket.emit('chatType', TypeChat, function(data){

			});
			$("#notificwrap").scrollTop($("#notificwrap").height());
		});

		socket.on('usernames', function(data){

        	$("#RegisteredList").empty();
        	const list = document.createElement('list');
        	        
        	for (var i = 0; i < data.length; i++) {

        		var body = data[i];

        		if (body !== name) {

        			const li = document.createElement('li');
    				li.setAttribute("data-chat", body);
	        		li.setAttribute("class", "list-group-item customFormLi");
	        		li.innerHTML = body;
	        		list.appendChild(li);

	        		(function(value){
	        			li.addEventListener("click", function(){

	        				socket.emit('send-user', value, function(data){
	     						
	        				});

	        				socket.emit('create', value);
	        				$('#privatemessages').empty();
	        				$('#prvivateoldermsg').empty();
	        				$('#ChatWrap').show();
	        				$('#PrivatemessagesWrap').show();						  	
						  	$('#SendmsgWrap').show();
						  	$("#PersonChat").show();
						  	$("#PersonChat").empty();
						  	$("#PersonChat").append(value);


	        				const chatDIVpvt = document.createElement('div');
	        				chatDIVpvt.setAttribute("data-chat", body);
	        				chatDIVpvt.setAttribute("id", body);
	        				chatDIVpvt.setAttribute("class", "ChatUser");
	        				messageContainer.append(chatDIVpvt);

	        			}, false)
	        		})(data[i]);
        		}
				        	
			}

			registeredList.appendChild(list);
        	
        })

        socket.on('olderNotifications', function(data) {
        	// appendMessage(`${data.sender}: ${data.message}`)
        	console.log(data);
        
		    for (var i = 0; i < data.length; i++){
		    	// We store html as a var then add to DOM after for efficiency
		    	// html += '<li>' + oldMessages[i].message + '</li>'
		    	const divoldnot = document.createElement('div');
		    	
		    	if(data[i].sender == name){

		    		divoldnot.innerHTML = data[i].notification +" : From You";
		    		divoldnot.setAttribute("class", "btn btn-info customNotification");

		    	}else{

		    		
		    		divoldnot.innerHTML = data[i].notification + " : From " + data[i].publisher;
		    		divoldnot.setAttribute("class", "btn btn-info customNotification");

		    	}
		    					        		
        		oldnotificationContainer.append(divoldnot);
        		$("#notificwrap").scrollTop($("#notificwrap").height());
		    }
		    
		})




		notificationform.addEventListener('submit', e => {
			e.preventDefault();

			const notificationmsg = notificationinput.value;

			if(notificationmsg.length !== 0 || notificationmsg.trim() !== ""){

       			const notificationElement = document.createElement('div');
	          	notificationElement.innerText = notificationmsg + " : From You";
	          	notificationElement.setAttribute("class", "btn btn-info customNotification");
	          	notificationContainer.append(notificationElement);
          	}

          	socket.emit('send-notification', notificationmsg, function(data) {
          		// here errorsinChat
          		console.log(data);
          		if (data.length == 0) {

          		}else{

          			$('#notificationError').html(data);
          			setTimeout(function () {
		            	$('#notificationError').html('');
		        	}, 3000);
		        	$("#notificwrap").scrollTop($("#notificwrap").height());	
          		}


          	})
          	notificationinput.value = ''
		})

		socket.on('notificationBroadcast', data => {

			$("#alertNotification").addClass("dot");
			$("#alertAll").addClass("dot");
			const notificationElement = document.createElement('div');
          	notificationElement.innerText = `${data.notification} : From ${data.name}`;
          	notificationElement.setAttribute("class", "btn btn-info customNotification");
          	notificationContainer.append(notificationElement);

		})


        // socket.on('user-connected', name => {
        // 	// const sessionID = socket.socket.sessionid;
        // 	// console.log(sessionid);
        //   	appendMessage(`${name} connected`)
        // })

        socket.on('olderMessages', function(data) {
        	// appendMessage(`${data.sender}: ${data.message}`)
        	console.log(data);
        
		    for (var i = 0; i < data.length; i++){
		    	// We store html as a var then add to DOM after for efficiency
		    	// html += '<li>' + oldMessages[i].message + '</li>'
		    	const divold = document.createElement('div');
		    	
		    	if(data[i].sender == name){

		    		divold.innerHTML = data[i].message;
		    		divold.setAttribute("class", "row messageSendclass");

		    	}else{

		    		
		    		divold.innerHTML = data[i].message;
		    		divold.setAttribute("class", "row messagereplyclass");

		    	}
		    					        		
        		pvtoldmessageContainer.append(divold);
		    }
		    $("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());
		    
		})
     	
     	socket.on('olderMessagespublic', function(data) {
        	// appendMessage(`${data.sender}: ${data.message}`)
        	console.log(data);
        
		    for (var i = 0; i < data.length; i++){
		    	// We store html as a var then add to DOM after for efficiency
		    	// html += '<li>' + oldMessages[i].message + '</li>'
		    	const divoldpub = document.createElement('div');
		    	
		    	if(data[i].sender == name){

		    		divoldpub.innerHTML = data[i].sender+": "+data[i].message;
		    		divoldpub.setAttribute("class", "row messageSendclass");

		    	}else{

		    		
		    		divoldpub.innerHTML = data[i].sender+": "+data[i].message;
		    		divoldpub.setAttribute("class", "row messagereplyclass");

		    	}
		    					        		
        		oldmessageContainer.append(divoldpub);
		    }
		    $("#messagesWrap").scrollTop($("#messagesWrap").height());
		    
		})


        socket.on('user-disconnected', function(data) {

        	console.log(data);
          	appendMessage(`${data} disconnected`)
        })



        messageForm.addEventListener('submit', e => {
          	e.preventDefault()
          	const message = messageInput.value
          	$("#alertPrivate").removeClass('dot');
          	$("#alertGroup").removeClass('dot');


          	if(message.length !== 0 || message.trim() !== ""){

          		if(TypeChat == 'pbl'){

          			appendMessage(`${message}`);

          		}else if(TypeChat == 'pvt'){

          			const meElement = document.createElement('div')
		          	meElement.innerText = `${message}`
		          	meElement.setAttribute("class", "messageSendclass");
		          	pvtmessageContainer.append(meElement)
		          	$("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());
          		}
          		
          	}

          	socket.emit('send-chat-message', message, function(data) {
          		// here errorsinChat
          		console.log(data);
          		if (data.length == 0) {

          		}else{

          			$('#errorsinChat').html(data);
          			setTimeout(function () {
		            	$('#errorsinChat').html('');
		        	}, 3000);
		        	$("#messagesWrap").scrollTop($("#messagesWrap").height());	
          		}


          	})
          	messageInput.value = ''
        })

        socket.on('chat-message', data => {

        	$("#alertGroup").addClass("dot");
        	$("#alertAll").addClass("dot");

          	// appendMessage(`${data.name}: ${data.message}`)
          	const element = document.createElement('div')
          	element.innerText = `${data.name}: ${data.message}`;
          	element.setAttribute("class", "messagereplyclass");
          	messageContainer.append(element)

          	$("#messagesWrap").scrollTop($("#messagesWrap").height());
        })

        socket.on('chat-message-private', function(data) {

        	$("#alertPrivate").addClass("dot");
        	$("#alertAll").addClass("dot");
        	// appendMessage(`${data.name}: ${data.message}`)
        	const messageEl = document.createElement('div');
          	messageEl.innerText = `${data.message}`;
          	// $(`#${data.name}`).append(messageEl);
          	messageEl.setAttribute("class", "messagereplyclass");
          	pvtmessageContainer.append(messageEl);
          	console.log(messageEl);

          	$("#PrivatemessagesWrap").scrollTop($("#PrivatemessagesWrap").height());

        })

        function appendMessage(message) {
          	const messageElement = document.createElement('div')
          	messageElement.innerText = message
          	messageElement.setAttribute("class", "messageSendclass");
          	messageContainer.append(messageElement)
          	$("#messagesWrap").scrollTop($("#messagesWrap").height());
        }

    </script>

</body>
<script>

	// function Duplicates(iterable) {
 //  		return new Set(iterable).size !== iterable.length;
	// }

	$(document).ready(function () {

		// $.ajax({

  //          type:'GET',
  //          url:'{{url("GetOnlineAgents")}}',
  //          data: {} ,
  //          success:function(data) {

  //          		var body = '';

  //             	$.each(data.Getusers, function (index, c) {

  //             		$( "#usersList" ).append($('<option>').val(c.id).text(c.username));
              		
  //            	});

  //          }
  //       });

  //       $('#usersList').on('change', function() {
	 //        var text = $('#usersList option:selected').text();

	 //        $('#SelectedAgents').val($('#SelectedAgents').val()+text+',');

	 //        var values = $('textarea#SelectedAgents').val();
	 //        var array = values.split(',');
	 //        var arrNew = array.filter(Boolean);
	 //        console.log(arrNew);
	 //        var output = Duplicates(arrNew);

	 //        if(output == true){

	 //        	alert ("Agents cannot be Duplicated");
	        
	 //        }


	 //        var data = JSON.stringify([arrNew]);

		// 	var xhr = new XMLHttpRequest();
		// 	// xhr.withCredentials = true;
			
		// 	xhr.addEventListener("readystatechange", function() {
		// 	  if(this.readyState === 4) {
		// 	    console.log(this.responseText);
		// 	  }
		// 	});

		// 	xhr.open("POST", document.location.protocol+'//'+document.location.host+":3000/agents");
		// 	xhr.setRequestHeader("Content-Type", "application/json");
		// 	// xhr.setRequestHeader('Access-Control-Allow-Origin', '*');

		// 	xhr.send(data);

	        
		// });

	    $('.badge').each(function (element) {
		if ($(this).html().trim()) {
		    $(this).removeClass('hidden');
		    $(this).siblings('.js_opacity').css('opacity', 1);
		} else {
		    $(this).siblings('.js_opacity').css('opacity', 0.7);
		}
	    });

	    var userType = '<?php echo Session::get('usertypeid') ?>';
	    if (userType == '17') {
		//alert("");
		setInterval(function () {
		    $.ajax({
			url: '{{url("count_sms_email_fax_rem")}}',
			type: 'GET',
			data: {},
			success: function (response)
			{

			    if (response.data_eml != 0) {
				document.getElementById("email").innerHTML = response.data_eml;
			    }

			    if (response.data_sms != 0) {
				document.getElementById("sms").innerHTML = response.data_sms;
			    }

			    if (response.data_fax != 0) {
				document.getElementById("fax").innerHTML = response.data_fax;
			    }
				if (response.data_rem != 0) {
				document.getElementById("rem").innerHTML = response.data_rem;
			    }

			    $('.badge').each(function (element) {
				if ($(this).html().trim()) {
				    $(this).removeClass('hidden');
				    $(this).siblings('.js_opacity').css('opacity', 1);
				} else {
				    $(this).siblings('.js_opacity').css('opacity', 0.7);
				}
			    });
			}
		    });
		}, 5000);
	    }
	});
    </script>

    <script>
	$("#add").on("click", function () {
	    $("<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>").appendTo(".content");
	});
    </script>
 <!-- Log Out reason Pop Up menu -->
    <div class="modal fade bd-example-modal-sm" id="logoutmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
	    <form class="form-horizontal" action="logout_agent" method="POST" id="add_clips3"
		  name="add_clips3" enctype="multipart/form-data">
                {{csrf_field()}}
		<div class="modal-content">
		    <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Select a Logout Reason</h5>
			<button type="button" onclick="refresh()" class="close" data-dismiss="modal" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		    </div>
		    <div class="modal-body" id="ticket_model">
			<select class="form-control  custom-select-value"  id="logoutreason" name="logoutreason">
				<?php foreach (session()->get('logout_reasons') as $value) { ?>
    			    <option value="<?php echo $value->status ?>">
				<?php echo $value->status ?></option>

<?php } ?>
			</select>
		    </div>
		    <div class="modal-footer" id="ticket_model_footer">
			<button type="submit"  style="width:30%;"
				class="btn btn-danger" >
			    Log Out</button>
		    </div>
		</div>
	    </form>
	</div>
    </div>

    <script>

	function logoutpopup() {
	    $('#logoutmodal').modal('show');
	}
	function goreminder() {
	location.href='{{url("remainderdashboard")}}';
}

    </script>
</html>
