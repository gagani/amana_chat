const express = require('express');
const cors = require('cors');

const mysql = require('mysql');

const app = express();
const http = require('http').Server(app);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});

// console.log(ip.address());

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "passw0rd",
    database: "phonikip_db"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

const users = {};
var isInitmessages = false;


io.on('connection', socket => {

    socket.on('new-user', function(data, callback) {

        callback(true);
        socket.nickname = data;
        users[socket.nickname] = socket;
        updateNames();
        // users[socket.id] = name;
        // socket.broadcast.emit('user-connected', name);
    })

    socket.on('send-user', function(data, callback) {

        var message = [];
        // body...
        socket.receiver = data;
        console.log(socket.receiver + "receiver");
        console.log(socket.nickname + "sender");
        // Old Messages
       
        // Initial app start, run db query
        con.query("SELECT * FROM `AgentChat` WHERE cast(`datetime` as Date) = cast(NOW() as Date) AND `sender` IN ('"+socket.nickname+"', '"+socket.receiver+"') AND `receiver` IN ('"+socket.nickname+"', '"+socket.receiver+"') AND `ChatType` = 'Private'")
        .on('result', function(data){
            // Push results onto the notes array
            message.push(data)
        })
        .on('end', function(){
            // Only emit notes after query has been completed
            // users[socket.receiver].emit('olderMessages', message)
            users[socket.nickname].emit('olderMessages', message)
        })
     
   
    })

    socket.onAny((event, ...args) => {
         console.log(event, args);
    });

    socket.on('chatType', function(data) {
        var message = [];
        var notifi = [];
        // body...
        socket.chatType = data;
        console.log(data);

        if (data == 'pbl') {

            // Initial app start, run db query
            con.query("SELECT * FROM `AgentChat` WHERE cast(`datetime` as Date) = cast(NOW() as Date) AND `ChatType` = 'Group'")
            .on('result', function(data){
                // Push results onto the notes array
                console.log(data);
                message.push(data)
            })
            .on('end', function(){
                // Only emit MESSAGES after query has been completed
                socket.emit('olderMessagespublic', message)                        
            })         
            
        }else if(data == 'group'){

            con.query("SELECT * FROM `AgentChatNotification` WHERE cast(`datetime` as Date) = cast(NOW() as Date)")
            .on('result', function(data){
                // Push results onto the notes array
                console.log(data);
                notifi.push(data)
            })
            .on('end', function(){
                // Only emit MESSAGES after query has been completed
                socket.emit('olderNotifications', notifi)                        
            })    
        }

    })

    socket.on('join', function(data) {
       console.log(data);
    });

    console.log(socket.receiver);


    socket.on('send-chat-message', function(data, callback) {

        var msg = data.trim();

        if (!msg == '') {

            if (socket.chatType == 'pvt') {
            
                if (socket.receiver in users) {

                    console.log('Private Message')
                    users[socket.receiver].emit('chat-message-private', { message: data, name: socket.nickname});

                    var sql = "INSERT INTO AgentChat (sender, receiver, message, ChatType, datetime) VALUES ('"+socket.nickname+"', '"+socket.receiver+"', '"+data+"', 'Private',NOW())";
                    con.query(sql, function (err, result) {
                        if (err) throw err;
                        console.log("Inserted Private Message Details");

                    });
                    // io.to(people[socket.receiver]).emit('chat message', data.msg);
                }else{
                    callback('Error: Enter a valid user!');
                }

                
                // socket.receiver.emit('chat-message', { message: data, name: socket.nickname})

            }else if(socket.chatType == 'pbl'){

                socket.broadcast.emit('chat-message', { message: data, name: socket.nickname})
                var query = "INSERT INTO AgentChat (sender, receiver, message, ChatType, datetime) VALUES ('"+socket.nickname+"', 'All', '"+data+"', 'Group',NOW())";
                con.query(query, function (err, result) {
                    if (err) throw err;
                    console.log("Inserted Group Message Details");
                });

            }

        }else{
            callback('Error: Message cannot be empty!');

        }
        
    })

    socket.on('send-notification', function(data, callback){
        var notification = data.trim();
        console.log(notification);
        if(notification !== ''){

            socket.broadcast.emit('notificationBroadcast', { notification: notification, name: socket.nickname}) 
            var sql = "INSERT INTO AgentChatNotification (notification, datetime, publisher) VALUES ('"+notification+"', NOW(), '"+socket.nickname+"')";
            con.query(sql, function (err, result) {
                if (err) throw err;
                console.log("Inserted Notification Details");
            });           

        }else{

            callback("Error: Notification cannot be empty!");
        }


    })


    function updateNames() {
        io.emit('usernames', Object.keys(users));
    }

    socket.on('disconnect', function(data) {
        
        if (!socket.nickname) return;
        delete users[socket.nickname];
        updateNames();

        // delete users[socket.id]
    })
})


http.listen(port, () => {
  	console.log(`Socket.IO server running at http://localhost:${port}/`);
});
